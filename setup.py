# -*- coding: utf-8 -*-
from mastodon import Mastodon

# Registering the App
Mastodon.create_app(
     'TTS_bot 🗣',
     api_base_url = 'https://botsin.space',
     to_file = 'client.secret'
)

# logging in
mastodon = Mastodon(
    client_id = 'client.secret',
    api_base_url = 'https://botsin.space'
)

mastodon.log_in(
    'email_adress@doamin.tld',
    'P4ssw0rd',
    to_file = 'user.secret'
)
