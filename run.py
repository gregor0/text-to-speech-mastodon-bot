# -*- coding: utf-8 -*-
"""
Created 2021-03-07

@author: @gregor@toot.cat
"""

# pip3 install Mastodon.py
from mastodon import Mastodon
from time import sleep as waitInSeconds

def speak(inText):
    from random import randint as randomInteger
    import subprocess
    
    espeakExec = r"espeak-1.48.04\command_line\espeak.exe"
    inFile = "text.txt"
    outFile = "speak.wav"

    with open(inFile, mode="wt") as fileHandle:
        fileHandle.write(inText)
    
    gap   = randomInteger(0  ,  5) # default is   0(?)
    speed = randomInteger(80 ,200) # default is 175
    pitch = randomInteger(0  , 99) # default is  50
    
    gap   =   1
    speed = 160
    pitch =  75
    
    call = f"{espeakExec} -f {inFile} -w {outFile} -g {gap} -s {speed} -p {pitch}"
    return subprocess.check_output(call)

mastodon = Mastodon(
    access_token = 'user.secret',
    api_base_url = 'https://botsin.space'
)
homeInstance = "botsin.space"

while True:
    try:
        print(".",end=" ")
        waitInSeconds(5)
        for notification in mastodon.notifications():
            waitInSeconds(5)
            notification_account = notification["account"]["acct"]
            if not ( "@" in notification_account):
                notification_account+="@"+homeInstance
            
            allowInteraction = True
            # Don't interact when they are a bot
            if notification["account"]["bot"]:
                allowInteraction = False
                print("BOT: " + notification["account"]["url"])
        
            # Don't interact when they have #nobot anywhere in the account
            badWords = ["nobot", "no_bot"]
            for word in badWords:
                if word in str(notification["account"]).lower() :
                    allowInteraction = False
                    print("#nobot: " + notification["account"]["url"]) 
                    
            # Don't interact if they dind't mention me
            if not "mention" == notification["type"]:
                print(notification["type"]+": "+notification["account"]["url"] )
                allowInteraction = False
            
            if not allowInteraction:
                continue
            
            print(notification["type"]+": "+notification["account"]["url"] )
            notification["created_at"]
            
            from re import sub as regExSub
            textContent = regExSub(r'<.*?>', '', notification["status"]["content"])
            
            speak(textContent)
            # mastodon.status_post(notification_account, in_reply_to_id = toot)
            filepath = "speak.wav"
            print(f"Uploading {filepath} ...")
            image = mastodon.media_post(filepath, description=f"What @{notification_account} just tooted.")
            print("Upload done, tooting ...")
            mastodon.status_post(status = "@"+notification_account, media_ids = image , in_reply_to_id = notification["status"]["id"])
        mastodon.notifications_clear()
    except Exception:
        waitInSeconds(30)
        # log in again
        mastodon = Mastodon(
            access_token = 'user.secret',
            api_base_url = 'https://botsin.space'
        )
        waitInSeconds(10)
            
        
